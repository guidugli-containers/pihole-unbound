#!/bin/bash

# Network ports for rootless user
IMAGE_PORTS="-p 1153:53/tcp -p 1153:53/udp -p 1180:80/tcp"

# Path to store systemd files (rootless)
SYSTEMD_PATH="$HOME/.config/systemd/user/"

DATADIR=/opt/pihole

if [ `id -u` -eq 0 ]; then
  # If running as root, publish the ports bellow
  IMAGE_PORTS="-p 53:53/tcp -p 53:53/udp -p 80:80/tcp"

  # Systemd path for root
  SYSTEMD_PATH="/etc/systemd/system/"
fi

mkdir -p $DATADIR/etc-dnsmasq.d $DATADIR/etc-pihole 

podman run \
	--detach=true \
	--name pihole-server \
	-v $DATADIR/etc-pihole:/etc/pihole:rw \
	-v $DATADIR/etc-dnsmasq.d:/etc/dnsmasq.d:rw \
	$IMAGE_PORTS \
	--memory=512M \
	--env-file=config.env \
	--restart=unless-stopped \
	docker.io/guidugli/pihole-unbound:latest

podman generate systemd -f -n pihole-server

mkdir -p $SYSTEMD_PATH

mv ./container-pihole-server.service $SYSTEMD_PATH

echo "Use systemctl to enable the container job that will run when the server restarts"
echo "For rootless container, enable linger for the user running the container"


