#!/bin/bash

iptables -t nat -F

iptables -t nat -I PREROUTING --source 0/0 --destination 0/0 -p tcp --dport 53 -j REDIRECT --to-ports 1153
iptables -t nat -I PREROUTING --source 0/0 --destination 0/0 -p udp --dport 53 -j REDIRECT --to-ports 1153
iptables -t nat -I OUTPUT -p tcp -o lo --dport 53 -j REDIRECT --to-ports 1153
iptables -t nat -I OUTPUT -p udp -o lo --dport 53 -j REDIRECT --to-ports 1153

