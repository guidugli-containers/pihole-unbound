#!/bin/bash

# Path to store systemd files (rootless)
SYSTEMD_PATH="$HOME/.config/systemd/user/"

if [ `id -u` -eq 0 ]; then
  # Systemd path for root
  SYSTEMD_PATH="/etc/systemd/system/"

  systemctl disable container-pihole-server.service

else
  # The command below may fail if the user has not the correct /run/user files
  # and environment variables. In this case user must troubleshoot the problem.
  systemctl --user disable container-pihole-server.service
  if [ $? -ne 0 ]; then
    echo "Failed to disable rootless container systemd service"
  fi
fi

podman stop pihole-server
podman rm pihole-server

rm -f $SYSTEMD_PATH/container-pihole-server.service

