#!/bin/sh

set -e

docker pull docker.io/pihole/pihole:latest

PIHOLE_VERSION=`docker inspect docker.io/pihole/pihole:latest --format '{{index .Config.Labels "org.opencontainers.image.version"}}'`

docker buildx create --use
docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 -t $CI_REGISTRY_IMAGE:$PIHOLE_VERSION --push .
docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 -t $CI_REGISTRY_IMAGE:latest --push .
