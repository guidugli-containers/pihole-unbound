#!/bin/bash

set -e

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
#export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

podman pull docker.io/pihole/pihole:latest

PIHOLE_VERSION=`podman inspect docker.io/pihole/pihole:latest --format '{{index .Config.Labels "org.opencontainers.image.version"}}'`

declare -A image
for plat in amd64 arm64 386 ppc64le arm/v7 arm/v8 ; do
  podman pull --platform linux/$plat docker.io/pihole/pihole:latest
  buildah build-using-dockerfile --no-cache --format docker --platform linux/$plat --iidfile iid .
  image[$plat]=$(cat iid)
done

rm -f iid

buildah manifest rm pihole-unbound:$PIHOLE_VERSION 2>/dev/null
buildah manifest create pihole-unbound:$PIHOLE_VERSION

for plat in ${!image[@]} ; do
  VARIANT=`echo "$plat/" | cut -d/ -f2`
  ARCH=`echo "$plat/" | cut -d/ -f1`
  flags=
  if [ -n "$VARIANT" ]; then
    flags="--arch $ARCH --variant $VARIANT"
  else
    flags="--arch $ARCH"
  fi
  buildah manifest add $flags pihole-unbound:$PIHOLE_VERSION ${image[$plat]}
done

buildah manifest push --all pihole-unbound:$PIHOLE_VERSION docker://guidugli/pihole-unbound:$PIHOLE_VERSION
buildah manifest push --all pihole-unbound:$PIHOLE_VERSION docker://guidugli/pihole-unbound:latest

