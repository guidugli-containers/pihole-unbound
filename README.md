# podman-pihole-unbound

This image uses pi-hole image and add unbound service to it,  based on [instructions provided directly by the Pi-Hole team](https://docs.pi-hole.net/guides/unbound/). This image works well with rootless podman.

## Usage

**Before Start:** Ensure that no other service is using port 53 (tcp and udp). One easy way to check is running: `lsof -i :53`
If the command returns blank, then there is no other service running on port 53. Usually you will need to disable systemd-resolved service (check for tutorials online on how to perform this task).

This image works identically as the original [docker pi-hole image](https://github.com/pi-hole/docker-pi-hole). This repository provides `config.env` file containing some variables to configure the image and a `create.sh` script that creates a (root/rootless) instance using podman, and adds systemd files to allow controlling if the container should start at reboot. 

### Enabling container autostart (root)

To enable container start on reboot, the user should run `systemctl enable <container-pihole-server.service>`.

### Enabling container autostart (rootless)

To enable container start on reboot, the user should run `systemctl --user enable <container-pihole-server.service>`.

Also, it is required to enable linger for the non-root user in order to auto-start the container on boot: `loginctl enable-linger "username"`

### Firewall configuration (rootless)

Running rootless container implies using non privileges network ports (higher than 1024). In order to use port 53 to resolve DNS, you need to add rules to iptables (or other firewall package) to redirect port 53 to the port exposed by the container. The file `iptables_rules.sh` have example on the rules needed for iptables. 

## License
BSD

